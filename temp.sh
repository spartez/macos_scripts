#!/bin/bash

_now=$(date +"%Y_%m_%d")
_file="temp_log_$_now.txt"

# osx-cpu-temp tools from GitHub
log_temp () {
    (date; ./osx-cpu-temp/osx-cpu-temp ; echo '-----------') | cat >> "$_file"
}


full_cpu_load () {
yes > /dev/null & yes > /dev/null & yes > /dev/null & yes > /dev/null &
}

echo 'Starting cpu stress test' > "$_file"
log_temp

full_cpu_load

#for (( ; ; ))
#do
#    log_temp
    # Wait  5 s
#    sleep 5
#done

ptty=$(stty --save)
stty -icanon min 0;
while true ; do
    if read -t 0; then # Input ready
        read -n 1 char
        echo -e "\nRead: ${char}\n"
        break
    else # No input
        echo -n '.'
	log_temp
        sleep 1
    fi       
done

killall yes

stty $ptty
