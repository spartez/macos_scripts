#!/bin/bash
#  (c) Atlassia Poland  2020
# Script for testing MDM env

# doing basic connectivity check
 sudo jamf checkJSSConnection

# Grabing local StaffID
localuser=$(whoami)

# And checking if it matches Jamf
sudo jamf listUsers -verbose | grep $localuser  &> /dev/null
if [ $? == 0 ]; then
    echo "User is present"
fi

#checkoing hostname
sudo jamf getComputerName


