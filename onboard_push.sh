#!/bin/bash
# Script for collecting data needed by Atlassian ZeroTrust VPN
# MacBook Serial Number , MAC address of WiFi interface and login name are neede in "Spartez Trusted Devices"
# 
#  (c) Spartez 2020

# Machine name
hostn=$(hostname)
shost='192.168.144.50'

# Local username  need to be exacly the same as Atlassian login
vpnuser=$(whoami)


# Get the same file
filen=$vpnuser.txt

# push file with vpn data  to server 
scp $filen  upload@$shost


