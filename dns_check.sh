#!/bin/bash
# Due to lots of issues with DNS on VPN Here is script 
# to do basic DNS health check on macOS
#
# (c) Atlassian Poland Sp. z o.o 2021

WEBHOST='atlassian.net'
DNS_LIST=$(scutil --dns | grep nameserver | cut -d ' ' -f 5 | sort | uniq)

echo "List of avialble nameservers present on current acitve connection"

echo $DNS_LIST \
| while read DNS_ADDRESS ; do	
    nslookup @WEBHOST @DNS+ADDRESS
    sleep 1
done

#nslookup @WEBHOST