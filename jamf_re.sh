#!/bin/bash
#  (c) Atlassia Poland  2021
# Script for doing manual re-enrollmet

# doing basic connectivity check
 sudo jamf checkJSSConnection

# Grabing local StaffID
localuser=$(whoami)

# And checking if it matches Jamf
sudo jamf listUsers -verbose | grep $localuser  &> /dev/null
if [ $? == 0 ]; then
    echo "User is present"
fi

# remove Machine form JAMF
sudo Jamf removeframework


# Start new enrollment
sudo profiles show -type enrollment

if [ $? == 0 ]; then
# Run Again if fail
 sudo profiles renew -type enrollment
 sudo profiles show -type enrollment

fi

