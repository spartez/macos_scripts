#!/bin/bash
# Script for Enabling Full Disk Encryption on MaBook wiht FileVault 
# We are using fdesetup utilty from MacOS
# 
#  (c) Spartez 2020

# Machine name
hostn=$(hostname)
vpnuser=$(whoami)
PASSWORD="install_password"

# Get the same file
filen=$vpnuser.txt

fdesetup stautus
fdesetup isactive

RECOVERY_KEY="12345"
fdsetup enable

# push file with vpn data  to server 
echo $RECOVERY_KEY >> $filen