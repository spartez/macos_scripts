#!/bin/bash
#  (c) Spartez 2020
# Script for installing essential software on fresh MacBook

# Installing VPN client (Not public sw, so need to provide binary package))
sudo /usr/sbin/installer -allowUntrusted -pkg ./anyconnect-macos-4.8.00175-core-vpn-webdeploy-k9.pkg -target / 

# Installing Chrome browser
DLURL="https://dl.google.com/chrome/mac/stable/GGRO/googlechrome.dmg"

temp=$TMPDIR$(uuidgen)

# Install Google Chrome

mkdir -p $temp/mount
curl $DLURL > $temp/1.dmg
yes | hdiutil attach -noverify -nobrowse -mountpoint $temp/mount $temp/1.dmg
cp -r $temp/mount/*.app /Applications
hdiutil detach $temp/mount
rm -r $temp

# Install Slack

#!/bin/bash
temp=$TMPDIR$(uuidgen)
mkdir -p $temp/mount
curl -L https://slack.com/ssb/download-osx -o $temp/1.dmg
yes | hdiutil attach -noverify -nobrowse -mountpoint $temp/mount $temp/1.dmg
cp -r $temp/mount/*.app /Applications
hdiutil detach $temp/mount
rm -r $temp

