#!/bin/bash
#  Script for installing IronWifi Certificates and setting up wifi
#  Original script by Magda K.  
#  Here we are starting to add new features and modify it for Catalina
#  (c) Spartez 2020
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

tempcert=ironca.pem
ironcacert=ironcacert.crt

IRONCA='-----BEGIN CERTIFICATE-----
MIIE1jCCA76gAwIBAgIJAIk+l0ubr8nWMA0GCSqGSIb3DQEBBQUAMIGQMQswCQYD
VQQGEwJVUzEQMA4GA1UECBMHRmxvcmlkYTEQMA4GA1UEBxMHT3JsYW5kbzERMA8G
A1UEChMISXJvbldpZmkxITAfBgkqhkiG9w0BCQEWEmhlbGxvQGlyb253aWZpLmNv
bTEnMCUGA1UEAxMeSXJvbldpZmkgQ2VydGlmaWNhdGUgQXV0aG9yaXR5MB4XDTE0
MDcxMDE5MDAwNloXDTI0MDUxODE5MDAwNlowgZAxCzAJBgNVBAYTAlVTMRAwDgYD
VQQIEwdGbG9yaWRhMRAwDgYDVQQHEwdPcmxhbmRvMREwDwYDVQQKEwhJcm9uV2lm
aTEhMB8GCSqGSIb3DQEJARYSaGVsbG9AaXJvbndpZmkuY29tMScwJQYDVQQDEx5J
cm9uV2lmaSBDZXJ0aWZpY2F0ZSBBdXRob3JpdHkwggEiMA0GCSqGSIb3DQEBAQUA
A4IBDwAwggEKAoIBAQCmVlll2rS9UMONhaZci5hdgql906a0DGEfPC8HlVUnWQSS
1JUYJu91dKo+BvkRaYEaC05frKxVtQAkveIqoi3/83nhb2bGNM4LZvZOHNoZqGyO
J2DWn3V4BlX3DeSExqsD3U9akdUyGyBjhfY0Ehvz57Zqa0lf66phdpZRNaamEw0M
dMHsmNRUWceF9JHQNjKwM0bx8j1r1HK1FZLCoeYSbqFzFplAlRMwtl/6y912s4Pd
JtgnlYz/3lWNWduCe7S6BxC/+btQIXjoHmG8zKx4x9R3sQebx+CIjqfzA5ldDuSg
wCI087cBPEADhpOfykSxi7JnDFxhYyqyvD8/KZiXAgMBAAGjggEvMIIBKzAdBgNV
HQ4EFgQUzGYg0Ir2zsfcn85IkX+brl24O5owgcUGA1UdIwSBvTCBuoAUzGYg0Ir2
zsfcn85IkX+brl24O5qhgZakgZMwgZAxCzAJBgNVBAYTAlVTMRAwDgYDVQQIEwdG
bG9yaWRhMRAwDgYDVQQHEwdPcmxhbmRvMREwDwYDVQQKEwhJcm9uV2lmaTEhMB8G
CSqGSIb3DQEJARYSaGVsbG9AaXJvbndpZmkuY29tMScwJQYDVQQDEx5Jcm9uV2lm
aSBDZXJ0aWZpY2F0ZSBBdXRob3JpdHmCCQCJPpdLm6/J1jAMBgNVHRMEBTADAQH/
MDQGA1UdHwQtMCswKaAnoCWGI2h0dHBzOi8vY29uc29sZS5pcm9ud2lmaS5jb20v
Y2EuY3JsMA0GCSqGSIb3DQEBBQUAA4IBAQA9sUj0p1k6GuCsdAKUJcSEOJtTRHgM
mNo/hT7tbgRvmVw8vNB1el7RkD1nyKsQHlOb7ErNh8FFtPQwE+nU3EYR9oE1hndJ
rmwBZk4sQ4vt7VJa3MApZjMgbfgPUtqS4CVGX2oK8kN/0vwUZdSfAlLZiLQ0TGH6
KwgGMrrxrFLmTxk37imRKhFI1L4UsnbHwB42ccOpU4y9TuN554AyPYWGGugL0Ke2
gRxJLYvfjYdJepX3y5OLQTPa/O2ZbXvXpm+H0rN3MjIFHtnrP74xyCnojvcvtYHU
dIfhoKmy+2ghmOdezitjI9nfKQ56ol50O4+5uc4se+dwCL1rpZt4Igwz
-----END CERTIFICATE-----' 
cat $IRONCA >> $tempcert 

openssl x509 -inform PEM -in $tempcert -out $ironcacert -outform DER

# text bold and normal
bold=$(tput bold)
normal=$(tput sgr0)

# enter current dir
cd $CURRENT_DIR

# asking user for certificate name and password
while
	unset cert
	while [ -z "${cert}" ]; do
		echo -n "Enter your certificate name: "
		read cert
	done

	echo "1. Searching for certificate location..."
	cert_dirs=`find /$HOME/Downloads/ -type f '(' -name $cert -o -name $cert.p12 ')' 2>/dev/null`
	cert_file=`echo $cert_dirs | cut -d " " -f1`

	while [ ! -f "$cert_file" ]; do
	    echo "File not found."
			read -e -p "Please provide full path to the certificate: " cert_file
			echo    # (optional) move to a new line
	done

	echo "File found at ${bold}$cert_file${normal} location!"

	unset pass
	while [ -z "${pass}" ]; do
		echo -n "Enter password to the certificate: "
		read pass
	done

	read -p "You will import certificate:${bold}$cert_file${normal} with password:${bold}$pass${normal}. Is that correct? [y/N]" -n 1 -r REPLY1
	echo    # (optional) move to a new line
do
	case $REPLY1 in
		([yY][eE][sS] | [yY]) break;;
    	(*) echo "Undo and try again...";;
  	esac
done

# validating the certificate and getting user gmail login at the same time
unset email
email="$(openssl pkcs12 -nokeys -password pass:$pass -in $cert_file | grep "spartez.com" | awk -F[=/] '{print $4}')"

if [ -z "${email}" ]; then
	echo "Couldn't read the certificate file. Probably because you entered ${bold}wrong password!${normal}"
	echo "Please start the script once more."
	exit 0
fi

echo "Certificate is signed for ${bold}$email${normal} user. Proceeding with the import..."

# deleting provious Spartez configuration
#echo "1. To delete previous SPARTEZ configuration profile, please enter your ${bold}system password${normal} first."
#sudo profiles list
#sudo profiles -R -p spartez -U $USER

# adding Iron WiFi CA certs
echo "2. Add Iron WiFi CA and server certificates to your System keychain. Please enter your ${bold}system password${normal} first."
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain $ironcacert
#sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain ironwifi.crt
#sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain ironwifi_server.pem

# import user certificate
echo "3. Import user certificte."
sudo security import ${cert_file} -A -x -k login.keychain -P ${pass}

# set network identity preferences to Spartez ssid
echo "4. Set network identity preferences to Spartez SSID."
#sudo networksetup -removepreferredwirelessnetwork en0 Spartez
#echo "Adding Spartez to preffered network list aa the first one."
wifi=`sudo networksetup -listallhardwareports | awk '/Hardware Port: Wi-Fi/,/Ethernet/' | awk 'NR==2' | cut -d " " -f 2`
sudo networksetup -addpreferredwirelessnetworkatindex $wifi Spartez 0 WPA2E
sudo security set-identity-preference -c "$email" -s "com.apple.network.eap.user.identity.wlan.ssid.Spartez" /Users/$USER/Library/Keychains/login.keychain
sudo security set-identity-preference -c "$email" -s "com.apple.network.eap.user.identity.default" /Users/$USER/Library/Keychains/login.keychain

# show network preferences
echo "5. List network preferences."
networksetup -listpreferredwirelessnetworks $wifi
read -p "THE END. Do you see any errors? [y/N]" -n 1 -r REPLY2
echo    # (optional) move to a new line
if [[ $REPLY2 =~ ^[Yy]$ ]]
   then
   echo "Please ${bold}copy and paste${normal} this output to your network administrator!"
else
   echo "${bold}So long, and thanks for all the fish!${normal}"
   exit 0
fi

