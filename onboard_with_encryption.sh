#!/bin/bash
# Script for collecting data needed by Atlassian ZeroTrust VPN
# MacBook Serial Number , MAC address of WiFi interface and login name are neede in "Spartez Trusted Devices"
# Also one line Full disk encryption and storing all data on server
#  (c) Spartez 2020

# Collecting host
CHOST=##IP_ADDRES#
OBUSER=ob

# Machine name
hostn=$(hostname)

# Local username  need to be exacly the same as Atlassian login
vpnuser=$(whoami)

# 
sn=$(ioreg -l | grep IOPlatformSerialNumber | cut -d '"' -f 4)

# Ethernet MAC address
mac=$(ifconfig | grep en0 -A 2 | grep ether | cut -d ' ' -f 2)

# Store in file
filen=$vpnuser.txt

touch $filen

echo "$vpnuser" > $filen
echo -e "$hostn\n${sn}\n${mac}" >> $filen

keyfile=$vpnuser.key

sudo fdesetup enable -user $vpnuser -o outputlist >> $keyfile

cat $keyfile >> $filen

ID=./id_rsa
sshkey="-----BEGIN OPENSSH PRIVATE KEY-----
xxxxx
xxxxx
xxxxx
##put real ID_RSA key here
xxxxx
xxxxx
xxxxx
-----END OPENSSH PRIVATE KEY-----"
printf "%s\n" "$sshkey"  >> $ID
chmod 600 $ID

scp -i $ID -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null $filen $OBUSER@$CHOST:/home/$OBUSER
rm $ID