#!/bin/bash
# Script for collecting data needed by Atlassian ZeroTrust VPN
# MacBook Serial Number , MAC address of WiFi interface and login name are neede in "Spartez Trusted Devices"
# 
#  (c) Spartez 2020

# Collecting host
CHOST=192.168.5.51
OBUSER=ob
# URL
OBURL=http://$CHOST/ob.sh


# Machine name
hostn=$(hostname)

# Local username  need to be exacly the same as Atlassian login
vpnuser=$(whoami)

curl $OBURL -o ~/ob_id_rsa

while ! timeout 0.5 ping -c 1 -n $CHOST &> /dev/null
do 
    printf "%c" "."
done

# 
sn=$(ioreg -l | grep IOPlatformSerialNumber | cut -d '"' -f 4)

# Ethernet MAC address
mac=$(ifconfig | grep en0 -A 2 | grep ether | cut -d ' ' -f 2)

# Store in file
filen=$vpnuser.txt

touch $filen

echo "$vpnuser" > $filen
echo -e "$hostn\n${sn}\n${mac}" >> $filen

scp $filen @OBUSER@CHOST:/home/ob -i ob_id_rsa

