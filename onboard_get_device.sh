#!/bin/bash
# Script for collecting data needed by Atlassian ZeroTrust VPN
# MacBook Serial Number , MAC address of WiFi interface and login name are neede in "Spartez Trusted Devices"
# 
#  (c) Spartez 2020

# Machine name
hostn=$(hostname)

# Local username  need to be exacly the same as Atlassian login
vpnuser=$(whoami)

# 
sn=$(ioreg -l | grep IOPlatformSerialNumber | cut -d '"' -f 4)

# Ethernet MAC address
mac=$(ifconfig | grep en0 -A 2 | grep ether | cut -d ' ' -f 2)

# Store in file
filen=$vpnuser.txt

touch $filen

echo "$vpnuser" > $filen
echo -e "$hostn\n${sn}\n${mac}" >> $filen


